---
layout: page
title: Sobre mi
permalink: /about/
---

Mi nombre es  **Francisco Mauriz Pereira**, naci en galicia pero resido en Sevilla.

Soy informático y amante de la tecnología, del software libre y demás gadget o artilugio que exista. Me gusta conocer las noticias del mundo digital, sus redes sociales y sus últimos avances.

Aunque también tengo mi lado no informático donde gasto las horas escuchando música y podcast (en este caso,hay mucho de escucha informática), leyendo animes, manga y libros de ciencia ficción y como no tengo remedio participando en algún curso online de programación... ¡ tengo que aprender algo sino me da algo !

#### ¿Sobre este blog?

Prentendo qe sea un lugar, donde la gente que le guste la tecnología, software libre, sistemas operativos, arduino y una gran variedad de temas puedan leer,compartir y si se dá el caso aprender uno de los otros.

#### Contacto

Si desean ponerse en contacto conmigo para alguna duda o sugerencia pueden hacerlo a través de los siguientes modos:

- Email: [franmaurizpereira@gmail.com](https://twitter.com/franmauriz)
- Twitter : [@franmauriz](https://twitter.com/franmauriz)
- Mastodon: [@fjmauriz](https://mastodon.social/@fjmauriz)